// 1. buat array of object siswa ada 5 data cukup
let siswa = {
    "nama" : "devi",
    "provinsi" : "jawa tengah",
    "umur" : 20,
    "status" : "single",
    "kesibukan" : "kuliah"
}

// 2. 3 function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak
function prov (namaProv){
    namaProv = siswa.provinsi;
    return namaProv == "jawa barat"? console.log("saya tinggal di jawa barat") : console.log("saya tidak tinggal di jawa barat")
}

function umurku(namaUmur) {
    namaUmur = siswa.umur;
    return namaUmur > 22? console.log("umur saya diatas 22 tahun") : console.log("umur saya dibawah 22 tahun")
}

function statusku(namaStatus){
    namaStatus = siswa.status;
    return namaStatus == "single"? console.log("status saya single") : console.log("status saya punya pasangan")
}
statusku()
umurku();
prov();


// 3. callback function untuk print hasil proses 3 function diatas.
// nama saya imam, saya tinggal di jawa baRAT, umur saya dibawah 22 tahun. dan status saya single loh. CODE buat Nilam
const siswaa = (callback) => {
    callback(siswa.nama, namaProv, namaUmur, namaStatus)
}
console.log("nama saya " + siswa.nama + "," + prov() + "," + umurku() + ". Dan" + statusku())

